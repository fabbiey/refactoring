package com.gildedrose;

class GildedRose {
    Item[] item;

    public GildedRose(Item[] items) {
        this.item = items;
    }

    public void updateQuality() {
        for (Item value : item) {
            processQuality(value);
            processSellIn(value);
            checkItemExpired(value);
        }
    }

    private void processQuality(Item item) {
        if (!item.name.equals("Aged Brie")
                && !item.name.equals("Backstage passes to a TAFKAL80ETC concert")) {
            if (item.quality > 0) {
                if (!item.name.equals("Sulfuras, Hand of Ragnaros")) {
                    item.quality--;
                }
            }
        } else if (item.quality < 50) {
            item.quality++;
            processQualityOfBackstagePasses(item);
        }
    }

    private void processQualityOfBackstagePasses(Item item) {
        if (item.name.equals("Backstage passes to a TAFKAL80ETC concert")) {
            increaseQualityExpiryLessThanTwoWeeks(item);
            increaseQualityExpiryLessThanOneWeek(item);
        }
    }

    private void increaseQualityExpiryLessThanOneWeek(Item item) {
        if (item.sellIn < 6) {
            incrementQuality(item);
        }
    }

    private void increaseQualityExpiryLessThanTwoWeeks(Item item) {
        if (item.sellIn < 11) {
            incrementQuality(item);
        }
    }

    private void incrementQuality(Item item) {
        if (item.quality < 50) {
            item.quality++;
        }
    }

    private void processSellIn(Item item) {
        if (!item.name.equals("Sulfuras, Hand of Ragnaros")) {
            item.sellIn--;
        }
    }

    private void checkItemExpired(Item item) {
        if (item.sellIn < 0) {
            processExpiredItem(item);
        }
    }

    private void processExpiredItem(Item item) {
        if (!item.name.equals("Aged Brie")) {
            if (!item.name.equals("Backstage passes to a TAFKAL80ETC concert")) {
                if (item.quality > 0) {
                    if (!item.name.equals("Sulfuras, Hand of Ragnaros")) {
                        item.quality--;
                    }
                }
            } else {
                item.quality = 0;
            }
        } else {
            incrementQuality(item);
        }
    }
}
